//Lo facciamo instansiabile (non static, perchè così possiamo fare l'oggetto Database è renderlo "Dinamico")
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.io.IOException;

public class Database {

  private Rubrica rubrica;
  private File salvataggio;
  private FileWriter writer;
  private Scanner reader;

  public Database(Rubrica rubrica) {
    this.rubrica = rubrica.clone();

    try {
      salvataggio = new File("salvataggi" + File.separator + rubrica.getName() + ".txt");
      salvataggio.createNewFile();
      reader = new Scanner(salvataggio);
      writer = new FileWriter(salvataggio, true);
    } catch(IOException e) {
      System.out.println("Impossibile generare il file richiesto");
    }
  }  

public void storeSaving() throws IOException {
  this.rubrica.forEach((contatto) -> {
    try {
      writer.write(contatto.getCategoria() + "\n");
      if(contatto instanceof Privato) {
        Privato privato = (Privato) contatto;
        writer.write(privato.getName() + "\n");
        writer.write(privato.getCognome() + "\n");
        writer.write(privato.getNumber() + "\n");
        writer.write(privato.getIgUsername() + "\n");
        writer.write(privato.isAmicoStretto() + "\n");
      } else if(contatto instanceof Azienda) {
        Azienda azienda = (Azienda) contatto;
        writer.write(azienda.getName() + "\n"); 
        writer.write(azienda.getNumber() + "\n");
        writer.write(azienda.getPIva() + "\n");
        writer.write(azienda.getEmail() + "\n");
      } else {
        Negozio negozio = (Negozio) contatto;
        writer.write(negozio.getName() + "\n");
        writer.write(negozio.getNumber() + "\n");
        writer.write(negozio.getWeb() + "\n");
        writer.write(negozio.getOrarioApertura() + "\n");
        writer.write(negozio.getOrarioChiusura() + "\n");
      }
      writer.write("---\n");
    } catch(IOException e) {
      System.out.print("Errore in scrittura");
    }
  });
  // Aiuta allo storeSaving
  this.writer.flush();
}

public void loadSaving() {
  String categoria;
  Contatto contatto;
  while(reader.hasNextLine()) {
    categoria = reader.nextLine();
    if(categoria.equalsIgnoreCase("privato")) {
      String[] privatoData = readLines(5);
      contatto = new Privato(
        privatoData[0],
        privatoData[1],
        privatoData[2],
        privatoData[3],
        Boolean.parseBoolean(privatoData[4])
      );
    } else if(categoria.equalsIgnoreCase("azienda")) {
      String[] aziendaData = readLines(4);
      contatto = new Azienda(
        aziendaData[0],
        aziendaData[1],
        aziendaData[2],
        aziendaData[3]
      );
    } else {
      String[] negozioData = readLines(5);
      contatto = new Negozio(
        negozioData[0],
        negozioData[1],
        negozioData[2],
        negozioData[3],
        negozioData[4]
      );
    } 
    this.rubrica.add(contatto);
    reader.nextLine();
  }
}

// Per leggere riga per riga
private String[] readLines(int nLines) {
    String[] lines = new String[nLines];
    for(int i = 0; i < nLines; i++) {
      lines[i] = reader.nextLine();
    }
    return lines;
  }
  public boolean isLoaded() {
    return this.rubrica != null;
  }
  //Nel set va lo store
  public void setRubrica(Rubrica rubrica) {
    this.rubrica = rubrica;
    try {
      this.writer.close();
      this.writer = new FileWriter(this.salvataggio);
      this.storeSaving();
    } catch (IOException e) {
      System.out.println("File eliminato !");
    }
  }
  //Nel get va il load
  public Rubrica getRubrica() {
    try {
      this.reader.close();
      this.reader = new Scanner(this.salvataggio);
    } catch(IOException e) {
      System.out.println("File eliminato !");
    }
    this.loadSaving();
    return this.rubrica.clone();
  }
}