import java.util.Arrays;
import java.util.function.Consumer;
import java.lang.Exception;

public class Rubrica {
  private static int counter = 0;

  private String nome;
  private int numContattiAggiunti = 0;
  private Contatto[] contatti;
  private int contPrivato = 0, contNegozio = 0, contAzienda = 0;


  public Rubrica(String nome, int maxContatti) {
    this.nome = nome;
    this.contatti = new Contatto[maxContatti];
    counter++;
  }
  public Rubrica(int maxContatti) {
    this("rubrica_" + counter, maxContatti);
  }

  public Contatto getContattoAt(int index) throws ContattoGiaEsisteException {
    //Contatto contatto = null; 
    //if (index >= 0 && index < numContattiAggiunti) {
    //  contatto = this.contatti[index];
    //} Uso le Exception per non dichiararlo null
    if(index < 0 || index >= numContattiAggiunti) {
      throw new ContattoGiaEsisteException("Index out of bounds");
    } 
    Contatto contatto = this.contatti[index];

    return contatto;
  }

  //Aggiunge nuovo contatto
  public Rubrica add(Contatto contatto) {
    if(numContattiAggiunti < this.contatti.length) {
      this.contatti[numContattiAggiunti] = contatto;
      this.numContattiAggiunti++;

      if(contatto instanceof Privato) {
        contPrivato++;
      } else if(contatto instanceof Negozio) {
        contNegozio++;
      } else {
        contAzienda++;
      }
    }
    return this;
  }
  //Stampa array list degli oggetti contatti
  // public void print() {
  //   for(int i = 0; i < numContattiAggiunti; i++) {
  //     System.out.println("- - - - - - -");
  //     System.out.println("Indice: " + (i) +
  //      "\n" + this.contatti[i]);
  //   }
  // }

  @Override
  public String toString() {
    String output = "";
    for (int i = 0; i < numContattiAggiunti; i++) {
      output = output + this.contatti[i].toString();
    }
    return output;
  }

  //Ordina i contatti presenti nella lista
  public void sortForNames() {
    if(numContattiAggiunti > 0) {
      Arrays.sort(this.contatti, 0, numContattiAggiunti); // numContattiAggiunti non viene contato qnd nn serve il - 1.
    }  
  }

  //Ricerca per nome
  // public void searchContactName(String contattoNome) {
  //   boolean found = false;
  //   Contatto c = null;
  //   for (int i = 0; i < numContattiAggiunti && !found; i++) {
  //     if(contatti[i].getName().equals(contattoNome)) {
  //       found = true;
  //       c = contatti[i];
  //     }
  //   }
  //   System.out.print(c);
  // }

  //Ricerca per numero
  // public void searchContactNumero(int contattoNumber) {
  //   boolean found = false;
  //   Contatto c = null;
  //   for (int i = 0; i < numContattiAggiunti && !found; i++) {
  //     if(contatti[i].getNumber() == contattoNumber) {
  //       found = true;
  //       c = contatti[i];
  //     }
  //   }
  //   System.out.print(c);
  // }

  //Ricerca per nome e numero
  public boolean search(String nome, String numero) {
    boolean found = false;
    for(int i = 0; i < numContattiAggiunti && !found; i++) {
      if(this.contatti[i].getName().equals(nome) && this.contatti[i].getNumber().equals(numero)){
        found = true;
      }
    }
    return found;
  }

  public void replace(int index, Contatto newContact) {
    if(index >= 0 && index < numContattiAggiunti) {
      this.contatti[index] = newContact;
    }
  }

  //Rimozione contatto attraverso il nome
  // public void removeContact(String name) { 
  //   for(int i = 0 ; i < numContattiAggiunti; i++) {
  //     if(contatti[i].getName().equals(name)) {
  //       this.remove(i);
  //     }
  //   }
  // }
  //Rimozione contatto attraverso l'index
  public void removeAt(int index) {
    if(index >= 0 && index <numContattiAggiunti){
    this.swap(index, numContattiAggiunti-1);
    this.removeLast();
    }
  }
  public void removeLast() {
    if(numContattiAggiunti > 0) {
      numContattiAggiunti--;
    }
  }
  public void swap(int i, int j){
    if(i >= 0 && i < numContattiAggiunti && j >=0 && j < numContattiAggiunti) {
      Contatto t = this.contatti[i];
      this.contatti[i] = this.contatti[j];
      this.contatti[j] = t;
    }
  }

  //Contatore di contatti fra le 3 tipologie
  public void printCounters() {
    //  for(int i = 0 ; i < numContattiAggiunti; i++) {
    //   if(contatti[i] instanceof Privato) {
    //     contPrivato++;
    //   } else if(contatti[i] instanceof Negozio) {
    //     contNegozio++;
    //   } else {
    //     contAzienda++;
    //   }
    //  }
     String sistemOut = "CONTATTI: " + numContattiAggiunti + "\n" + "di cui:" + "PRIVATO: "  + contPrivato + "\n NEGOZIO: " + contNegozio + "\n AZIENDA: " + contAzienda;
     System.out.println(sistemOut);
  }

  public void printWithIndexes() {
    for(int i = 0; i < this.numContattiAggiunti; i++) {
      System.out.println(i + ")" + this.contatti[i].getName() + " " + this.contatti[i].getNumber());
    }
  }

  public Rubrica clone() {
    Rubrica other = new Rubrica(this.nome, this.contatti.length);
    for(int i = 0; i < this.numContattiAggiunti; i++) {
        other.add(this.contatti[i].clone());
    }
    return other;
    }


  public void forEach(Consumer<Contatto> action) {
    for (int i = 0; i < numContattiAggiunti; i++) {
      action.accept(this.contatti[i]);
    }
  }

  public String getName() {
    return this.nome;
  }
}