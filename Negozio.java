import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class Negozio extends Contatto {
  
  private static DateFormat dateFormat = new SimpleDateFormat("HH:mm");

  private String web;
  private Date orarioApertura;
  private Date orarioChiusura;

  public Negozio(String nome, String numero, String web, String orarioApertura, String orarioChiusura) {
    super(nome, numero);
    this.categoria = "NEGOZIO";
    this.web = web;
    try {
      this.orarioApertura = dateFormat.parse(orarioApertura);
      this.orarioChiusura = dateFormat.parse(orarioChiusura);
    } catch (ParseException e) {
      System.out.print("Errore nell'inserimento dell'orario");
      System.exit(1);
    }
  }

  @Override
  public String toString() {
    return super.toString() +
           "SITO WEB: " + this.web + "\n" +
           "APERTURA: " + dateFormat.format(orarioApertura) + "\n" + 
           "CHIUSURA: " + dateFormat.format(orarioChiusura) + "\n";
  }

  @Override
  public Contatto clone() {
    Negozio other = new Negozio(this.nome, this.numero, this.web, dateFormat.format(this.orarioApertura), dateFormat.format(this.orarioChiusura));
    return other;
  }

  public String getWeb() {
    return this.web;
  }

  public String getOrarioApertura() {
    return dateFormat.format(orarioApertura);
  }
  public String getOrarioChiusura() {
    return dateFormat.format(orarioChiusura);
  }

}