public abstract class Contatto implements Comparable {
  
  protected String nome; 
  protected String numero;
  protected String categoria = "";  

  public Contatto(String nome, String numero) {
    this.setName(nome);
    this.setNumber(numero);  
  }

  @Override
  public String toString() {
    return "- - - - - - - - -" + "\n" + 
           "CATEGPRIA: " + this.categoria + "\n" +
           "NOME: " + this.nome + "\n" + 
           "NUMERO: " + this.numero + "\n";
  }

  @Override
  public int compareTo(Object o) {
    Contatto other = (Contatto) o;
    return this.nome.compareTo(other.getName());
  }

  @Override
  public boolean equals(Object o) {
    Contatto other = (Contatto) o;
    return this.nome.equals(other.getName()) && this.numero.equals(other.numero) && this.categoria.equals(other.categoria);
  }

  public abstract Contatto clone();

  public String getCategoria() {
    return this.categoria;
  }

  public String getName() {
    return this.nome;
  }
  public void setName(String nome) {
    this.nome = nome;
  }
  public String getNumber() {
    return this.numero;
  }
  public void setNumber(String numero) {
    this.numero = numero;
  }
}