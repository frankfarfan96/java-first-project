public class ContattoGiaEsisteException extends Exception {
  public ContattoGiaEsisteException(String msg) {
    super(msg);
  }
}