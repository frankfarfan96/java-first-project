import java.time.LocalTime;

public class Azienda extends Contatto {
  private String pIva;
  private String email; 
  
  public Azienda(String nome, String numero, String pIva, String email) {
    super(nome, numero);
    this.pIva = pIva;
    this.email = email;
    this.categoria = "AZIENDA";
  }
  
  @Override
  public String toString() {
    return super.toString() +
           "P.IVA: " + this.pIva + "\n" +  
           "EMAIL: " + this.email + "\n";
  }
  @Override
  public Contatto clone() {
    Azienda other = new Azienda(this.nome, this.numero, this.pIva, this.email);
    return other;
  }

  public String getPIva() {
    return this.pIva;
  }
  public String getEmail() {
    return this.email;
  }
}