public class Privato extends Contatto {
  private String cognome;
  private String igUsername;
  private boolean amicoStretto;

  public Privato(String nome, String cognome, String numero, String igUsername, boolean amicoStretto) {
    super(nome, numero);
    this.cognome = cognome;
    this.igUsername = igUsername;
    this.amicoStretto = amicoStretto;
    this.categoria = "PRIVATO";
  }

  @Override
  public String toString() {
    //Abbiamo fatto in questo modo perche così potevamo scegliere l'odrine di posizionamento
    return "- - - - - - - - - - -" + "\n" +
           "CATEGPRIA: " + this.categoria + "\n" +
           "NOME: " + this.nome + "\n" + 
           "NUMERO: " + this.numero + "\n" + 
           "INSTAGRAM: " + this.igUsername + "\n" + 
           "AMICO STRETTO: " + (this.amicoStretto ? "SI" : "NO") + "\n";
  }

  @Override
  public  Contatto clone() {
    Privato other = new Privato(this.nome, this.cognome, this.numero, this.igUsername, this.amicoStretto);
    return other;
  }

  public void setAmicoStretto(boolean amicoStretto) {
    this.amicoStretto = amicoStretto;
  }
  public boolean isAmicoStretto() {
    return this.amicoStretto;
  } 
  public String getIgUsername() {
    return this.igUsername;
  }
  public void setIgUsername(String igUsername) {
    this.igUsername = igUsername;
  }
  public String getCognome() {
    return this.cognome;
  }
  public void setCognome(String cognome) {
    this.cognome = cognome;
  }

}