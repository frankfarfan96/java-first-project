import java.util.Scanner;
  
public class InterazioneUtente {
  //Cos'ì lo possiamo usare in tutti i metodi di questa classe
  private static Scanner sc = new Scanner(System.in);
 public static void printMenu() {
    String menu = """
         - - - - - - - Rubrica - - - - - - - - -
         Seleziona una funzione:
         0) Stampare menu
         
         1) Stampare contatti
         2) Aggiungere contatto
         3) Cerca contatto 
         4) Rimuovere contatto
         5) Modificare un contatto
         6) Ordina contatti (per nome) 

         10) Salva su Database
         11) Carica da Database

         99) Uscire
         - - - - - - - - - - - - - - - - - - - - - - - 
    """;
    System.out.println(menu);
}

  public static void gestisciRubrica(Rubrica rubrica) throws Exception {
    Database db = new Database(rubrica);
    int option; 
    do {
      rubrica.printCounters();
      printMenu();
      option = askForNumber();

      switch(option) {
        case 0:
          rubrica.printCounters();
          printMenu();
          break;

        case 1:
          System.out.println(rubrica);
          break;
        case 2:
          gestisciAggiuntaContatto(rubrica);
          break;
        case 3:
          gestisciRicercaContatto(rubrica);
          break;
        case 4:
          printInfo("Seleziona il contatto da eliminare: \n");
          rubrica.printWithIndexes();
          int indexForDelete = askForNumber();
          rubrica.removeAt(indexForDelete);
          break;
        case 5:
          printInfo("Seleziona il contatto da modificare: \n");
          rubrica.printWithIndexes();
          int indexForEdit = askForNumber();
          gestisciModificaContatto(indexForEdit, rubrica);
          break;
        case 6:
          rubrica.sortForNames();
          break;

        case 10:
          db.setRubrica(rubrica);
          break;
          
        case 11:
          if(db.isLoaded()) {
            rubrica = db.getRubrica();
          }
          break;

        case 99:
          printInfo("Arrivederci !!");
          break;
        default:
          printError("Opzione non consentita");
          break;
      }

    System.out.print("\n [ENTER] per continuare...");
    keyboard.nextLine();
    System.out.println();
    
    } while (option != 99);
  }

  private static void gestisciModificaContatto (int index, Rubrica rubrica) throws Exception {
   
    Contatto contatto = rubrica.getContattoAt(index);
   
    if(contatto instanceof Privato) {
      Privato nuovoPrivato = creaContattoPrivato();
      rubrica.replace(index, nuovoPrivato);
    } else if(contatto instanceof Azienda) {
      Azienda nuovaAzienda = creaContattoAzienda();
      rubrica.replace(index, nuovaAzienda);
    } else {
      Negozio nuovoNegozio = creaContattoNegozio();
      rubrica.replace(index, nuovoNegozio);
    }
  }

  private static void gestisciRicercaContatto(Rubrica rubrica) {
    System.out.println("Inserisci nome e numero separati da spazio");
    String line = askForInput();
    String[] searchParameters = line.split(" ");
    if (rubrica.search(searchParameters[0], searchParameters[1])) {
      System.out.println("il conttatto con nome " + searchParameters[0] + " esiste");
    } else {
      System.out.println("il conttatto con nome " + searchParameters[0] + " NON esiste");
    }
  }

  // public static String getUtenteName() {
  //     sc.nextLine();
  //     System.out.print(" SCRIVI IL TUO NOME: ");
  //     return sc.nextLine();
  // }
 
  private static void gestisciAggiuntaContatto(Rubrica rubrica){
    String menu = """
        Scegli la tipologia di contatto da aggiungere ...

        p) Privato
        a) Azienda
        n) Negozio

        q) torna al menu
      
      """;

      System.out.println(menu);

      String option = askForInput();

      switch(option){
        case "A":
        case "a":
          Azienda azienda = creaContattoAzienda();
          rubrica.add(azienda);
          break; 

        case "P":
        case "p":
          Privato privato = creaContattoPrivato();
          rubrica.add(privato);
          break;

        case "N":
        case "n":
          Negozio negozio = creaContattoNegozio();
          rubrica.add(negozio);
          break;
      }
  }

  private static String askForField(String field) {
    System.out.print(field + ": ");
    return sc.nextLine();
  }

  
  private static String askForInput() {
    System.out.print("~~> ");
    String val = sc.nextLine();
    return val;
  }
  private static Integer askForNumber() {
    System.out.print("~~> ");
    Integer val = sc.nextInt();
    sc.nextLine();
    return val;
  } 

  // private static void creaContatto(Contatto toFill){
  //   String name = askForField("NOME");
  //   String phone = askForField("TEL");
  //   toFill.setName(name);
  //   toFill.setPhone(phone);
  // }

  private static Privato creaContattoPrivato() {
    String name = askForField("NOME");
    String surname = askForField("COGNOME");
    String phone = askForField("TEL");
    String ig = askForField("IG USERNAME");
    boolean closeFriend = askForField("AMICO STRETTO (s/n)").equalsIgnoreCase("s") ? true : false;
    Privato nuovoContatto = new Privato(name, surname, phone, ig, closeFriend);

    return nuovoContatto;
  }
  private static Azienda creaContattoAzienda() {
    String name = askForField("NOME");
    String phone = askForField("TEL");
    String partitaIva = askForField("P.IVA");
    String email = askForField("EMAIL");
 
    Azienda nuovoContatto = new Azienda(name, phone, partitaIva, email);

    return nuovoContatto;
  }
  private static Negozio creaContattoNegozio() {
    String name = askForField("NOME");
    String phone = askForField("TEL");
    String web = askForField("SITO WEB");
    String orarioApertura = askForField("ORARIO APERTURA (HH:mm)");
    String orarioChiusura = askForField("ORARIO CHIUSURA (HH:mm)");
    Negozio nuovoContatto = new Negozio(name, phone, web, orarioApertura, orarioChiusura);

    return nuovoContatto;
  }

  public static void printError(String error) {
      System.out.println(Colors.ANSI_RED + error + Colors.ANSI_RESET);
  }
  public static void printInfo(String info) {
      System.out.println(Colors.ANSI_BLUE + info + Colors.ANSI_RESET);
  }
}
