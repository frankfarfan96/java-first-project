import java.time.LocalTime;

public class Main {

    public static void main(String[] args) throws Exception {
      System.out.println();

      Rubrica rubrica = new Rubrica("amici", 20);

      InterazioneUtente.gestisciRubrica(rubrica);

      //PROVE PER I METODI DELLA CLASSE RUBRICA
        // Privato amico1 = new Privato("Pepe", 333, "pepe123", true);
        // Negozio negozio1 = new Negozio("Maciellaio", 336, "I00000662194ZZ", "maciellaio@123.it");
        // Azienda azienda1 = new Azienda("King Company", 339, "www.kingcompany.com", LocalTime.of(9, 0), LocalTime.of(18, 0));
        // Azienda azienda2 = new Azienda("Princes Company", 339, "www.kingcompany.com", LocalTime.of(9, 0), LocalTime.of(18, 0));
      
        // rubrica.add(amico1).print();
        // rubrica.add(negozio1).print();
        // rubrica.add(azienda1).print();
        // rubrica.add(azienda2).print();
        // rubrica.contatoreDiContatti();
        // rubrica.searchContactNumero(339);
        // rubrica.removeContact("Pepe");
        // rubrica.print(); 
        // rubrica.add(amico1);
        // rubrica.ordinaContatti();
        // rubrica.print();
    }
}
